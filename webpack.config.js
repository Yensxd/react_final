const path = require('path');

// plugins y minificadores de css yscss/sass
// para reducir el tamaño de las hojas ded estilo de nuestro proyectos
const HtmlWebpackPlugin = require('html-webpack-plugin'); // para el templa del html que va a usar webpack
const MiniCssExtractPlugin = require('mini-css-extract-plugin'); // para reducir los css
const { SourceMapDevToolPlugin } = require('webpack'); // para conocer el source de nuestro proyecto

// configuracion de puerto
const port = process.env.PORT || 3000;

// exportar configuracion de webpack
module.exports = {
    entry: './src/index.jsx',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'bundle.[hash].js',
        publicPath: '/',
    },
    context: path.resolve(__dirname),
    devServer: {
        port,
        historyApiFallback: true,
    },
    devtool: 'eval-source-map',
    module: {
        rules: [
            // reglas para archivos e js y jsx*
            // tiene que pasar el linting
            {
                enforce: 'pre',
                test: /(\.js|\.jsx)$/,
                exclude: /node_modules/,
                use: [
                    'eslint-loader',
                    'source-map-loader',
                ],
            },
            // reglas para archivos js y jsx
            // reglas de babel es y jsx
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/env',
                            '@babel/react',
                        ],
                    },
                },
            },
            // reglas para archivos css ysas y scss para minificarlos y cargarlos en el bundle
            {
                test: /(\.css|\.scss|\.sass)$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' },
                ],
            },
            // regla para los archivos de imagenes
            {
                test: /\.(png|jpe?g|gif)$/,
                use: [
                    { loader: 'file-loader' },
                ],
            },

        ],
    },
    plugins: [
        // template html
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: './css/styles.css',
        }),
        new SourceMapDevToolPlugin({
            filename: '[file].map',
        }),
    ],

    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss', '.sass'],
        modules: [
            'node_modules',
        ],
        alias: {
            'react-redux': path.join(__dirname, '/node_modules/react-redux/dist/react-redux.min'),
        },
    },
};
