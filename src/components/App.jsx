import React from 'react';
// https://gitlab.com/masajo/ob-react-final/-/blob/main/.eslintrc.json
/**
 * Función anónima para crear un componente principal
 * @returns {React.Component} componente principal de nuestra aplicación
 */
 const App = () => {
  return (
    <div>
      <h1>Proyecto  Final React JS</h1>
    </div>
  );
};

export default App;
