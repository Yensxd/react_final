import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

//  import redux
//  import {Provider} from 'react-redux';

//  importando las hojas de estilos
// import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/css/index.scss';

// TODO si trabajamos con redux, crear el streo y aplicar el middleare de redux saga

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
    document.getElementById('root'),
);
